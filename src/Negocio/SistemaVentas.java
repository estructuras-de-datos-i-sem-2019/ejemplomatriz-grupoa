/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Vendedor;
import Util.ExceptionUFPS;
import Util.LeerMatriz_Excel;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Clase del sistema de ventas
 * @author madarme
 */
public class SistemaVentas {
    
    private Vendedor equipoVentas[];

    public SistemaVentas() {
     
    }

    public SistemaVentas(int numVendedores) {
        
        this.equipoVentas=new Vendedor[numVendedores];
     
    }
    
    /**
     *  Constructor que carga los vendedores a partir de un excel
     * @param rutaArchivo un string con la ruta y el nombre del archivo Ejemplo: src/Datos/vendedores.xls
     * @throws IOException Genera excepción cuando el archivo no existe
     */
    
    public SistemaVentas(String rutaArchivo) throws IOException, ExceptionUFPS
    {
        LeerMatriz_Excel myExcel=new LeerMatriz_Excel(rutaArchivo,0);
        String datos[][]=myExcel.getMatriz();
        
        // Normalizar --> Pasar de la matriz de String al modelo del negocio (equipoVentas con cada uno de sus vendedores
        this.equipoVentas=new Vendedor[datos.length-1];
        crearVendedores(datos);
    
    }
    
    
    private void crearVendedores(String datos[][]) throws ExceptionUFPS
    {
    
     for(int fila=1;fila<datos.length;fila++)
     {
         //Crear un Vendedor
         Vendedor nuevo=new Vendedor();
         //Vector de ventas: Creando el espacio
         float ventas[]=new float[datos[fila].length-2];
         
         int indice_venta=0;
         
         for(int columna=0;columna<datos[fila].length;columna++)
         {
         
             if(columna==0)
                 nuevo.setCedula(Long.parseLong(datos[fila][columna]));
             else
             {
                 if(columna==1)
                     nuevo.setNombre(datos[fila][columna]);
                 else //Default
                 {
                     
                     try
                      {
                        float valorNota=Float.parseFloat(datos[fila][columna]);;
                            if(valorNota<0)
                                throw new ExceptionUFPS("Su valor de ventas es negativo, por lo tanto no se puede crear el objeto de la clase Vendedor");

                        ventas[indice_venta]=valorNota;
                        indice_venta++;

                        }catch(java.lang.NumberFormatException ex)
                            {
                             throw new ExceptionUFPS("Las ventas sólo aceptan datos númericos:"+ex.getMessage());
                            }
                     
                     
                 }
             }
             
         }
         //Asignar el vector de ventas al vendedor:
         nuevo.setVentas(ventas);
         //Asingar el nuevo vendedor al equipo de vendedores:
         this.equipoVentas[fila-1]=nuevo;
      }
    
    }
    
    public Vendedor[] getEquipoVentas() {
        return equipoVentas;
    }

    public void setEquipoVentas(Vendedor[] equipoVentas) {
        this.equipoVentas = equipoVentas;
    }

    @Override
    public String toString() {
        
        String msg="";
            for(Vendedor myVendedor:this.equipoVentas)
                     msg+=myVendedor.toString()+"\n";
        
        return msg;
        
    }
    
    /**
     * Una colección de vendedores que obtuvieron ventas mayores al promedio total de ventas
     * @return  una colección de vendedores
     */
    public Vendedor[] getVendedores_MasVentas()
    {
        return null;
    }
    
    /**
     * Obtiene el nombre de la venta que obtuvo la menor ganancia
     * venta1, ... venta6
     * @return 
     */
    public String getVenta_Menor()
    {
        return "";
    }
    
   /**
    * Obtiene el vendedor que obtuvo la menor venta acumulada
    * @return un objeto de tipo vendedor
    */
    public Vendedor getMenosVentas()
    {
        return null;
    }
    
    
    public String []getPromedioVentas()
    {
    //1. Vector de ventas tendrá la cantidad de vendedores:
     String resultado[]=new String[this.equipoVentas.length];
     // Ejemplo: Cada posición del vector: "Pepe perez, $3.456.000
     
     for(int i=0;i<resultado.length;i++)
         resultado[i]="Nombre:"+this.equipoVentas[i].getNombre()+",Ventas:"+this.equipoVentas[i].getPromedioVentas();
     
     return resultado;
    }
    
    
    public void crearInforme_PDF() throws Exception
    {
    String resultado[]=this.getPromedioVentas();
    //1. Crear el objeto que va a formatear el pdf:
    Document documento = new Document();
    //2. Crear el archivo de almacenamiento--> PDF
     FileOutputStream ficheroPdf = new FileOutputStream("src/Datos/ficheroSalida.pdf");
     //3. Asignar la estructura del pdf al archivo físico:
     PdfWriter.getInstance(documento,ficheroPdf);
     documento.open();
     Paragraph parrafo = new Paragraph();
     parrafo.add("Está es mi matriz de Vendedores:");
     
     Paragraph parrafo2 = new Paragraph();
     parrafo2.add(this.toString());
     
     
     
     Paragraph parrafo3 = new Paragraph();
     parrafo3.add("Listado de Vendedores con sus promedios de venta");
     
     //Vamos a crear la tabla: Se debe crear con la cantidad de columnas
     PdfPTable tabla = new PdfPTable(1);
     boolean sw=true;
     for(String datoPromedio:resultado)
     {
     PdfPCell celda=new PdfPCell(new Phrase(datoPromedio));
     if(sw)
         celda.setBackgroundColor(BaseColor.DARK_GRAY);
     else
         celda.setBackgroundColor(BaseColor.LIGHT_GRAY);
     
     sw=!sw;
     tabla.addCell(celda);
     }
             
             
             
             
             
     documento.add(parrafo);
     documento.add(parrafo2);
     documento.add(parrafo3);
     documento.add(tabla);
     
     documento.close();
    }
    
    
}
